package auth

import (
	"github.com/gofiber/fiber/v2"
	data "gitlab.com/redplanetpub/auth-golang.git/data"
	"go.mongodb.org/mongo-driver/mongo"
)

func InitModule(fiberApp *fiber.App, mongoClient *mongo.Client, database string) {
	data.SetupFiber(fiberApp, mongoClient, database)
}
