package presentation

import (
	mongoRepository "gitlab.com/redplanetpub/auth-golang.git/data/mongo"
	network "gitlab.com/redplanetpub/auth-golang.git/data/network"
	model "gitlab.com/redplanetpub/auth-golang.git/modules/auth/domain"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

type AuthRoute interface {
	InitializeModule(app *fiber.App, client *mongo.Client, database string)
}

func InitializeModule(app *fiber.App, client *mongo.Client, database string) {
	useCase := NewAuthUseCase()

	mongoRepository.Client = client

	collection := "client"
	app.Use(network.InitCollection(database, collection), network.ValidateStruct(&model.Client{}))

	app.Post("/signup", useCase.Signup)
	app.Post("/login", useCase.Login)
	app.Post("/forgotpassword", useCase.ForgotPassword)
	app.Post("/forgotpasswordvalidatetoken", useCase.ForgotPasswordValidateToken)
	app.Put("/forgotpasswordupdate", useCase.ForgotPasswordUpdate)
}
