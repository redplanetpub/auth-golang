package presentation

import (
	"encoding/json"
	"fmt"
	"net/smtp"

	notification "gitlab.com/redplanetpub/auth-golang.git/common/manager/notification"

	mongo "gitlab.com/redplanetpub/auth-golang.git/data/mongo"
	network "gitlab.com/redplanetpub/auth-golang.git/data/network"
	model "gitlab.com/redplanetpub/auth-golang.git/modules/auth/domain"

	"github.com/gofiber/fiber/v2"
)

type AuthController struct {
}

func (uc *AuthUseCase) Signup(c *fiber.Ctx) error {
	data := new(model.Client)
	data.ID = model.ID()
	network.BodyParse(c, data)

	err := mongo.InsertOne(data)
	if err != nil {
		return network.ErrorHandling(c, "Error al obtener información")
	}

	return network.Success(c, data, "OK")
}

func (uc *AuthUseCase) Login(c *fiber.Ctx) error {
	data := new(model.Client)
	network.BodyParse(c, data)

	dataLogin := new(model.Client)
	key := "email"
	err := mongo.FindOneToLogin(key, data.Email).Decode(&dataLogin)

	if err != nil {
		return network.ErrorHandling(c, "El correo que ingresaste no está registrado como usuario")
	}

	if data.Password != dataLogin.Password {
		return network.ErrorHandling(c, "La constraseña que ingresaste es incorrecta, inténtalo de nuevo")
	} else {
		return network.Success(c, dataLogin, "Inicio de sesión exitoso")
	}

}

func (uc *AuthUseCase) ForgotPassword(c *fiber.Ctx) error {
	data := new(model.Client)
	network.BodyParse(c, data)

	dataLogin := new(model.Client)

	key := "email"
	errEmail := mongo.FindOneToLogin(key, data.Email).Decode(&dataLogin)
	fmt.Println(errEmail)
	if errEmail != nil {
		return network.ErrorHandling(c, "El correo que ingresaste no está registrado como usuario")
	}

	token, _ := notification.GenerateToken(data.Email)
	smtpServer := "smtp.gmail.com"
	smtpPort := 587
	senderEmail := "devnotificacionesmx@gmail.com"
	senderPassword := "wmodnmggxpxxksku"
	recipientEmail := []string{data.Email}

	auth := smtp.PlainAuth("", senderEmail, senderPassword, smtpServer)

	claims, errToken := notification.ValidateToken(token)
	if errToken != nil {
		fmt.Println("Error al desencriptar el token:", errToken)
	}

	htmlBody := notification.TemplateHtmlForgotPassword(claims.Otp)
	message := notification.CreateMIMEMessage(senderEmail, recipientEmail, "Rag - Recupera tu contraseña", htmlBody)

	err := smtp.SendMail(fmt.Sprintf("%s:%d", smtpServer, smtpPort), auth, senderEmail, recipientEmail, message)
	if err != nil {
		return network.ErrorHandling(c, "Error al enviar el correo")

	}

	res := map[string]interface{}{
		"token": token,
		"email": claims.Email,
		"otp":   claims.Otp,
	}

	return network.Success(c, res, "Correo enviado correctamente.")
}

func (uc *AuthUseCase) ForgotPasswordValidateToken(c *fiber.Ctx) error {

	data := new(notification.Claims)
	network.BodyParse(c, data)

	// Desencriptar el token y obtener las claims
	claims, err := notification.ValidateToken(data.Token)
	if err != nil {
		return network.ErrorHandling(c, "")
	}
	fmt.Printf("OtpRquest: %s\n", data.Otp)
	fmt.Printf("OtpToken: %s\n", claims.Otp)

	if data.Otp != claims.Otp {
		return network.ErrorHandling(c, "Otp incorrecto")

	}

	res := map[string]interface{}{
		"token":      claims.Token,
		"email":      claims.Email,
		"isValidate": true,
	}

	return network.Success(c, res, "Token válido")
}

func (uc *AuthUseCase) ForgotPasswordUpdate(c *fiber.Ctx) error {
	token := c.Query("authToken")

	_, errToken := notification.ValidateToken(token)
	if errToken != nil {
		return network.ErrorHandling(c, "")
	}

	data := new(model.Client)
	network.BodyParse(c, data)

	var updateData map[string]interface{}

	if err := json.Unmarshal(c.Body(), &updateData); err != nil {
		return err
	}
	key := "email"

	err := mongo.UpdateOneToForgotPassword(key, data.Email, updateData)
	if err != nil {
		return network.ErrorHandling(c, "Error al obtener información")
	}

	return network.Success(c, updateData, "Contraseña actualizada correctamente")
}
