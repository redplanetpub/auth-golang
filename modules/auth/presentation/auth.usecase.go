package presentation

import (
	mongoRepository "gitlab.com/redplanetpub/auth-golang.git/data/mongo"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

type AuthUseCase struct {
	collection *mongo.Collection
}

func NewAuthUseCase() *AuthUseCase {
	return &AuthUseCase{
		collection: mongoRepository.Collection,
	}
}

type AuthInterface interface {
	Signup(c fiber.Ctx) error
	Login(c fiber.Ctx) error
	ForgotPassword(c fiber.Ctx) error
	ForgotPasswordValidateToken(c fiber.Ctx) error
	ForgotPasswordUpdate(c fiber.Ctx) error
}
