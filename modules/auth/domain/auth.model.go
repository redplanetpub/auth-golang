package domain

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Client struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Name     string             `json:"name"`
	Email    string             `json:"email" validate:"required,email"`
	Password string             `json:"password" validate:"omitempty,min=8"`
}

func ID() primitive.ObjectID {
	return primitive.NewObjectID()
}
