package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	Client     *mongo.Client
	Collection *mongo.Collection
)

type GenericModel struct {
	Elements []interface{}
}

func InitializeModule(database string, col string) {
	Collection = Client.Database(database).Collection(col)
}

func InsertOne(data interface{}) error {
	_, err := Collection.InsertOne(context.TODO(), data)
	if err != nil {
		return err
	}
	return err
}

func FindOneToLogin(key string, value string) *mongo.SingleResult {
	filter := bson.M{key: value}
	resp := Collection.FindOne(context.TODO(), filter)

	return resp
}

func UpdateOneToForgotPassword(key string, value string, data map[string]interface{}) error {

	filter := bson.M{key: value}

	updateData := bson.M{}

	for key, value := range data {
		updateData[key] = value
	}

	_, err := Collection.UpdateOne(context.TODO(), filter, bson.M{"$set": updateData})
	if err != nil {
		return err
	}
	return err
}
