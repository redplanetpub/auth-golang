package network

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/redplanetpub/auth-golang.git/data/mongo"
)

func BodyParse(c *fiber.Ctx, data interface{}) error {
	err := c.BodyParser(data)
	if err != nil {
		return err
	}
	return err
}

func ValidateStruct(data interface{}) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var validate = validator.New()
		bodyParserErr := BodyParse(c, data)
		if bodyParserErr != nil {
			return ErrorHandling(c, "Error al obtener información")
		}
		if err := validate.Struct(data); err != nil {
			return ErrorHandling(c, "Datos incompletos")
		}
		return c.Next()
	}
}

func InitCollection(database string, collection string) fiber.Handler {
	return func(c *fiber.Ctx) error {
		mongo.InitializeModule(database, collection)
		return c.Next()
	}
}
