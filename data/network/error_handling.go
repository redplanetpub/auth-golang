package network

import (
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
)

type ErrorCode int

const (
	ErrBadRequest ErrorCode = iota
	ErrUnauthorized
	ErrForbidden
	ErrNotFound
	ErrInternalServerError
)

var ErrorCodes = map[ErrorCode]int{
	ErrBadRequest:          fiber.StatusBadRequest,
	ErrUnauthorized:        fiber.StatusUnauthorized,
	ErrForbidden:           fiber.StatusForbidden,
	ErrNotFound:            fiber.StatusNotFound,
	ErrInternalServerError: fiber.StatusInternalServerError,
}

var ErrorMessages = map[ErrorCode]string{
	ErrBadRequest:          "Datos de solicitud incorrectos",
	ErrUnauthorized:        "No autorizado",
	ErrForbidden:           "Prohibido",
	ErrNotFound:            "Registro no encontrado",
	ErrInternalServerError: "Error interno del servidor",
}

func ErrorHandling(c *fiber.Ctx, message string, errorCode ...ErrorCode) error {
	var code ErrorCode
	if len(errorCode) > 0 {
		code = errorCode[0]
	} else {
		code = ErrBadRequest
	}

	msg := ErrorMessages[code]
	if len(strings.TrimSpace(message)) == 0 {
		message = msg
	}

	httpCode, exists := ErrorCodes[code]
	if !exists {
		httpCode = ErrorCodes[ErrBadRequest]
	}

	return Error(c, message, httpCode)
}

func Error(c *fiber.Ctx, message string, code int) error {
	response := APIResponse{
		Success:   false,
		Message:   message,
		Code:      code,
		Timestamp: time.Now().Format(time.RFC3339),
	}
	return c.Status(code).JSON(response)
}
