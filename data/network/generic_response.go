package network

import (
	"reflect"
	"time"

	"github.com/gofiber/fiber/v2"
)

type APIResponse struct {
	Success   bool        `json:"success"`
	Message   string      `json:"message,omitempty"`
	Code      int         `json:"code,omitempty"`
	Data      interface{} `json:"data,omitempty"`
	Timestamp string      `json:"timestamp"`
}

func Success(c *fiber.Ctx, data interface{}, message string) error {
	response := APIResponse{
		Success:   true,
		Message:   message,
		Data:      data,
		Timestamp: time.Now().Format(time.RFC3339),
		Code:      c.Response().StatusCode(),
	}
	dataType := reflect.TypeOf(data)
	if dataType.Kind() == reflect.Slice {
		if reflect.ValueOf(data).Len() == 0 {
			response.Data = make([]interface{}, 0)
		}
	} else {
		if data == nil {
			response.Data = make(map[string]interface{})
		}
	}
	return c.JSON(response)
}
