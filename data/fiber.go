package data

import (
	authRoute "gitlab.com/redplanetpub/auth-golang.git/modules/auth/presentation"

	"github.com/gofiber/fiber/v2"

	"go.mongodb.org/mongo-driver/mongo"
)

var (
	App      *fiber.App
	Database *mongo.Database
)

type Fiber interface {
	SetupFiber(fiberApp *fiber.App, client *mongo.Client, database string)
}

func SetupFiber(fiberApp *fiber.App, client *mongo.Client, database string) {
	authRoute.InitializeModule(fiberApp, client, database)
}
