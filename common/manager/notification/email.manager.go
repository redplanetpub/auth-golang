package notification

import (
	"fmt"
	"strings"
)

type EmailManager interface {
	CreateMIMEMessage(from string, to []string, subject, body string) []byte
	TemplateHtmlForgotPassword(otp string) string
}

func CreateMIMEMessage(from string, to []string, subject, body string) []byte {
	// Crear un mensaje MIME multipart/alternative
	msg := &strings.Builder{}
	msg.WriteString(fmt.Sprintf("From: %s\r\n", from))
	msg.WriteString(fmt.Sprintf("To: %s\r\n", strings.Join(to, ",")))
	msg.WriteString(fmt.Sprintf("Subject: %s\r\n", subject))

	mimeWriter := strings.Builder{}
	mimeWriter.WriteString("MIME-version: 1.0\r\n")
	mimeWriter.WriteString("Content-Type: multipart/alternative; boundary=boundary123\r\n")
	mimeWriter.WriteString("\r\n")

	// Parte de texto
	mimeWriter.WriteString("--boundary123\r\n")
	mimeWriter.WriteString("Content-Type: text/plain; charset=UTF-8\r\n")
	mimeWriter.WriteString("\r\n")
	mimeWriter.WriteString("Da clic en este enlace para cambiar tu contraseña\r\n")
	mimeWriter.WriteString("\r\n")

	// Parte HTML
	mimeWriter.WriteString("--boundary123\r\n")
	mimeWriter.WriteString("Content-Type: text/html; charset=UTF-8\r\n")
	mimeWriter.WriteString("\r\n")
	mimeWriter.WriteString(body)
	mimeWriter.WriteString("\r\n")

	// Cierre del mensaje
	mimeWriter.WriteString("--boundary123--\r\n")

	msg.WriteString(mimeWriter.String())
	return []byte(msg.String())
}

func TemplateHtmlForgotPassword(otp string) string {
	return fmt.Sprintf(`
	<html>
		<head>
			<style>
				body {
					font-family: 'Arial', sans-serif;
					background-color: #f4f4f4;
					margin: 0;
					padding: 20px;
				}
				.container {
					background-color: #fff;
					border-radius: 10px;
					box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
					padding: 20px;
				}
				.header {
					background-color: #2c3e50; /* Gris oscuro */
					border-top-left-radius: 10px;
					border-top-right-radius: 10px;
					color: #fff;
					padding: 10px;
					text-align: center;
				}
				.content {
					color: #000; /* Negro */
					font-size: 16px;
					text-align: center;
				}
				.otp-container {
					margin-top: 10px;
					text-align: center;
				}
				.otp {
					background-color: #ecf0f1; /* Gris claro */
					padding: 10px;
					border-radius: 5px;
					font-weight: bold;
					font-size: 20px; /* Tamaño de letra más grande */
					color: #000;
					display: inline-block;
				}
				.footer {
					background-color: #2c3e50; /* Gris oscuro */
					border-bottom-left-radius: 10px;
					border-bottom-right-radius: 10px;
					color: #fff;
					padding: 10px;
					text-align: center;
				}
			</style>
		</head>
		<body>
			<div class="container">
				<div class="header">
					<p>Rag - Recuperación de Contraseña</p>
				</div>
				<div class="content">
					<p>¡Hola!</p>
					<p>Hemos recibido una solicitud para restablecer la contraseña de tu cuenta en Rag. Si no has solicitado esto, puedes ignorar este mensaje.</p>
					<div class="otp-container">
						<p>Código OTP para recuperación de contraseña:</p>
						<span class="otp">%s</span>
					</div>
					<p>Este código es válido por un tiempo limitado. No compartas este código con nadie por razones de seguridad.</p>
					<p>¡Gracias por confiar en nosotros!</p>
				</div>
				<div class="footer">
					<p>Saludos,<br>Equipo Rag</p>
				</div>
			</div>
		</body>
	</html>
	`, otp)
}
