package notification

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

func GenerateToken(email string) (string, error) {
	mySigningKey := []byte("MyKey")

	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	randomNumber := r.Intn(900000) + 100000

	claims := &Claims{
		Email: email,
		Otp:   strconv.Itoa(randomNumber),
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Minute * 5)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			NotBefore: jwt.NewNumericDate(time.Now()),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func ValidateToken(tokenString string) (*Claims, error) {
	mySigningKey := []byte("MyKey")

	// Parsea el token y verifica la firma
	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*Claims)
	if !ok || !token.Valid {
		return nil, fmt.Errorf("token no válido")
	}

	if time.Now().Unix() > claims.RegisteredClaims.ExpiresAt.Unix() {
		return nil, fmt.Errorf("token caducado")
	}

	return claims, nil
}

type Claims struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Otp      string `json:"otp"`
	Token    string `json:"token"`
	jwt.RegisteredClaims
}
