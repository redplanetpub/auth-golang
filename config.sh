#!/usr/bin/env bash

go env -w GOPRIVATE=gitlab.com

git config \
  --global \
  url."git@gitlab.com:".insteadOf \
  "https://gitlab.com"
