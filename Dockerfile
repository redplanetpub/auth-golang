# syntax=docker/dockerfile:1

FROM golang:1.21.3

WORKDIR /app

COPY . ./
RUN ls
RUN go mod tidy


# Build
RUN CGO_ENABLED=0 GOOS=linux go build -o /rag-gitlab.com/redplanetpub/auth-golang.git

EXPOSE 8080

# Run
CMD ["/rag-gitlab.com/redplanetpub/auth-golang.git"]
